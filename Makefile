FASMG=fasmg

all: examples/test.bin

examples/%.bin:
	$(FASMG) $< $@

examples/test.bin: examples/test/test.asm examples/common/font.inc asm/femto8.inc
