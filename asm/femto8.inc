define femto8

macro femto8.do_addr opcode, operand, allow_imm
	local ln
	match [n=,=x?], operand
		ln = n
		db opcode + 3
		dw ln
	else match *[n=,=x?], operand
		ln = n
		db opcode + 7
		dw ln
	else match *[n]=,=x?, operand
		ln = n
		db opcode + 5
		db ln
	else match *[n], operand
		ln = n
		if ln <= 0xFF
			db opcode + 4
			db ln
		else
			db opcode + 6
			dw ln
		end if
	else match [n], operand
		ln = n
		if ln <= 0xFF
			db opcode + 0
			db ln
		else
			db opcode + 2
			dw ln
		end if
	else
		if allow_imm = 0
			err 'immediate value not allowed'
		end if
		ln = operand
		db opcode + 1
		if allow_imm = 1
			db ln
		else
			dw ln
		end if
	end match
end macro

macro femto8.do_branch opcode, operand
	local ln, lm
	ln = operand
	lm = $ + 2
	if ln and 0xFF00 = lm and 0xFF00
		db opcode + 0
		db ln and 0x00FF
	else
		db opcode + 1
		dw ln
	end if
end macro

macro femto8.do_jump opcode, operand, same_page
	local ln, lm
	lm = $ + 2
	match n=,=x?, operand
		ln = n
		db opcode + 3
		dw ln
	else match [n=,=x?], operand
		ln = n
		db opcode + 7
		dw ln
	else match [n]=,=x?, operand
		ln = n
		db opcode + 5
		db ln
	else match [n], operand
		ln = n
		if ln <= 0xFF
			db opcode + 4
			db ln
		else
			db opcode + 6
			dw ln
		end if
	else match n, operand
		ln = n
		if (same_page = 0 & ln <= 0xFF) | (same_page = 1 & ln and 0xFF00 = lm and 0xFF00)
			db opcode + 0
			db ln and 0xFF
		else
			db opcode + 2
			dw ln
		end if
	else match #n, operand
		ln = operand
		db opcode + 1
		db ln
	else
		err 'invalid operand'
	end match
end macro

macro femto8.addr name, opcode, allow_imm:1
	esc macro name operand&
		femto8.do_addr opcode, <operand>, allow_imm
	esc end macro
end macro

macro femto8.rmw name, opcode
	esc macro name operand&
		match , operand
			db opcode + 1
		else
			femto8.do_addr opcode, <operand>, 0
		end match
	esc end macro
end macro

macro femto8.jump name, opcode, same_page:0
	esc macro name operand&
		femto8.do_jump opcode, <operand>, same_page
	esc end macro
end macro

macro femto8.branch name, opcode
	esc macro name operand&
		femto8.do_branch opcode, <operand>
	esc end macro
end macro

macro femto8.instr name, opcode
	esc macro name
		db opcode
	esc end macro
end macro

femto8.addr ld?, 000q
femto8.addr st?, 010q

femto8.addr ldx?, 020q, 2
femto8.addr stx?, 030q, 2
femto8.addr ldxa?, 040q

femto8.addr cmp?, 050q
femto8.addr sub?, 060q
femto8.addr sbc?, 070q

femto8.addr add?, 100q
femto8.addr adc?, 110q

femto8.addr bit?, 120q

femto8.addr and?, 130q
femto8.addr or?, 140q
femto8.addr xor?, 150q

femto8.instr nop?, 161q
femto8.jump jmp?, 160q, 1
femto8.jump call?, 170q

femto8.rmw inc?, 200q
femto8.rmw dec?, 210q

femto8.rmw shl?, 220q
femto8.rmw shr?, 230q
femto8.rmw sar?, 240q

femto8.rmw not?, 250q

femto8.rmw rlc?, 260q
femto8.rmw rrc?, 270q

femto8.branch jnc?, 300q
femto8.branch jnv?, 302q
femto8.branch jnz?, 304q
femto8.branch jns?, 306q

femto8.branch jc?, 310q
femto8.branch jv?, 312q
femto8.branch jz?, 314q
femto8.branch js?, 316q

femto8.instr clc?, 320q
femto8.instr cli?, 321q
femto8.instr clv?, 322q
femto8.instr cld?, 323q
femto8.instr clz?, 324q
femto8.instr clx?, 325q
femto8.instr cls?, 326q
femto8.instr cla?, 327q

femto8.instr stc?, 330q
femto8.instr sti?, 331q
femto8.instr stv?, 332q
femto8.instr std?, 333q
femto8.instr stz?, 334q
femto8.instr ldsx?, 335q
femto8.instr sts?, 336q
femto8.instr stsx?, 337q

femto8.instr ret?, 340q
femto8.instr iret?, 341q

femto8.instr inx?, 342q
femto8.instr dex?, 343q

femto8.instr txs?, 344q
femto8.instr tsx?, 345q

femto8.instr brk?, 346q
femto8.instr wai?, 347q

femto8.instr php?, 350q
femto8.instr phx?, 351q
femto8.instr txal?, 352q
femto8.instr txah?, 353q
femto8.instr plp?, 354q
femto8.instr plx?, 355q
femto8.instr taxl?, 356q
femto8.instr taxh?, 357q
femto8.instr cxz?, 360q

K_LEFT = 0x01
K_UP = 0x02
K_RIGHT = 0x04
K_DOWN = 0x08
K_START = 0x10
K_A = 0x20
K_B = 0x40
K_C = 0x80

SYS_MAP01 = 0x4000
SYS_MAP23 = 0x4001
SYS_MAPC = 0x4002

SYS_SERIAL_OUT = 0x4004

SYS_CTRL0 = 0x4008
SYS_CTRL1 = 0x4009
SYS_CTRL2 = 0x400A
SYS_CTRL3 = 0x400B

AGU_C0BASE = 0x4020
AGU_C0LEN = 0x4021
AGU_C0SPEED = 0x4022
AGU_C0VOL = 0x4023

AGU_C1BASE = 0x4024
AGU_C1LEN = 0x4025
AGU_C1SPEED = 0x4026
AGU_C1VOL = 0x4027

AGU_C2BASE = 0x4028
AGU_C2LEN = 0x4029
AGU_C2SPEED = 0x402A
AGU_C2VOL = 0x402B

AGU_C3BASE = 0x402C
AGU_C3LEN = 0x402D
AGU_C3SPEED = 0x402E
AGU_C3VOL = 0x402F

PPU_WAIT = 0x4040
PPU_SCROLLX = 0x4042
PPU_SCROLLY = 0x4043

CHR_RAM = 0x2000
MAP_RAM = 0x3000
SPRITE_RAM = 0x4100
ATTR_RAM = 0x4200
PAL_RAM = 0x4400
AUDIO_RAM = 0x4800

ROM_SIZE_8K = 0
ROM_SIZE_16K = 1
ROM_SIZE_32K = 2
ROM_SIZE_64K = 3
ROM_SIZE_128K = 4

H_ROM_SIZE = ROM_SIZE_8K

macro rom_header
	db "FEMTO-8", 0
	db (H_ROM_SIZE shl 0)
	db 0, 0, 0, 0, 0, 0, 0
end macro

macro rom_end
	repeat $$ + (0x2000 shl H_ROM_SIZE) - $
		db 0
	end repeat
end macro

macro chr_tile_line t
	local lt, b, n1, n2, j
	lt = t

	n1 = 0
	n2 = 0
	repeat 8, i:0
		j = 7 - i
		b = lt mod 10
		lt = lt / 10
		n1 = n1 or ((b shr 0 and 1) shl j)
		n2 = n2 or ((b shr 1 and 1) shl j)
	end repeat

	db n1, n2
end macro

macro chr_tile_empty n:1
	local ln
	ln = n
	repeat 16 * ln
		db 0
	end repeat
end macro
