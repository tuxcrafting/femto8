# FEMTO-8 Specification

# Memory Map

- 0x0000-0x1FFF - Main RAM
- 0x2000-0x2FFF - Character RAM
- 0x3000-0x33FF - Map RAM
- 0x4000-0x40FF - IO space
- 0x4100-0x41FF - Sprite RAM
- 0x4200-0x43FF - Attribute RAM
- 0x4400-0x441F - Palette RAM
- 0x4800-0x48FF - Audio RAM
- 0x8000-0xFFFF - Program ROM

Unspecified areas may or may not work, don't use them.

# System

The system in and of itself is clocked at 4.608 MHz
(300&times;256&times;60 or 12000&times;384).

## IO ports

- 0x4000 - `SYS_MAP01` - W
  - Bits 0-3 - Bank index of 0x8000-0x9FFF
  - Bits 4-7 - Bank index of 0xA000-0xBFFF
- 0x4001 - `SYS_MAP23` - W
  - Bits 0-3 - Bank index of 0xC000-0xDFFF
  - Bits 4-7 - Bank index of 0xE000-0xFFFF
- 0x4008 - `SYS_CTRL0` - R - Controller 0
- 0x4009 - `SYS_CTRL1` - R - Controller 1
- 0x400A - `SYS_CTRL2` - R - Controller 2
- 0x400B - `SYS_CTRL3` - R - Controller 3
  - Bit 0 - D-pad left
  - Bit 1 - D-pad up
  - Bit 2 - D-pad right
  - Bit 3 - D-pad down
  - Bit 4 - Start
  - Bit 5 - A
  - Bit 6 - B
  - Bit 7 - C
- 0x4004 - `SYS_SERIAL_OUT` - W - Debug character output (may or may not work)

# CPU

The FEMTO-8 CPU is an 8-bit little-endian processor inspired by
the 6502. Its clock is divided by 2 from the system clock, giving
2.304 MHz.

## Registers

There are 6 registers:

- A - 8-bit - Main accumulator
- X - 16-bit - Index register
- SP - 16-bit - Stack pointer
- PC - 16-bit - Program counter (address of the NEXT instruction)
- F - 8-bit - Flags register
  - Bit 0 - C - Arithmetic carry
  - Bit 1 - V - Signed overflow
  - Bit 2 - Z - Equal to zero
  - Bit 3 - S - Sign bit
  - Bit 4 - I - Interrupt enable
  - Bit 5 - D - Decimal mode
  - Bit 6 - Always 1
  - Bit 7 - Always 1
- PSW - 16-bit - High 8 bits are A, low 8 bits are F

## Bus timing

Instructions normally do one 8-bit memory access per clock cycle, only
having wait states while the processor is waiting for an interrupt.

## Reset and interrupts

During interrupts, PC and then PSW is pushed on the stack, PC is set
to 0xFF03, and C and D are cleared.

After reset, the processor has the following state:

- PSW = 0x0000
- X = 0x0000
- SP = 0x0200
- PC = 0xFF00

## Addressing modes

- `op n` - 8-bit immediate (+1) - The effective address is the byte
  next to the opcode.
- `op nn` - 16-bit immediate (+1) - The effective address is the two
  bytes next to the opcode.
- `op` - Implied (+1) - In read-modify-write instructions, the
  accumulator.
- `op [n]` - 8-bit absolute address (+0) - Address inside zero-page
  (0x0000-0x00FF), or in the case of `jmp`, address inside PC's
  current page.
- `op [nn]` - 16-bit absolute address (+2) - Absolute address.
- `op *[n]` - 8-bit indirect address (+4) - Points to a pair of bytes
  in zero-page containing the effective address.
- `op *[nn]` - 16-bit indirect address (+6) - Same as 8-bit indirect
  but the pair of bytes is anywhere within memory.
- `op [nn,x]` - Indexed (+3) - 16-bit absolute address indexed by X.
- `op *[n],x` - Indirect indexed (+5) - Indirect address in zero-page,
  indexed by X.
- `op *[nn,x]` - Indexed indirect (+7) - Indirect address, with the
  address of the address being indexed by X.

Note that immediate addressing modes also work with instructions that
store to memory, in which case it will store the data inside the
immediate slot. It's an artifact of the decoding, and I can't really
see any specific use for it, but life finds a way or something.

Additionally, branches have 2 addressing modes:

- 8-bit intra-page (+0), only modifies low byte of PC.
- 16-bit (+1), sets PC entirely.

## Instructions

| Instruction | Opcode (octal) | Addressing modes         | Flags affected | Description                                                  |
|-------------|----------------|--------------------------|----------------|--------------------------------------------------------------|
| `ld`        | 000            | All, w/ 8-bit immediate  | Z, S           | Load a value inside A                                        |
| `st`        | 010            | All, w/ 8-bit immediate  |                | Store A in memory                                            |
| `ldx`       | 020            | All, w/ 16-bit immediate |                | Load a value inside X                                        |
| `stx`       | 030            | All, w/ 16-bit immediate |                | Store X in memory                                            |
| `ldxa`      | 040            | All, w/ 8-bit immediate  |                | Load an address within X                                     |
| `cmp`       | 050            | All, w/ 8-bit immediate  | C, V, Z, S     | Subtract A and a value, discard the result but not the flags |
| `sub`       | 060            | All, w/ 8-bit immediate  | C, V, Z, S     | Subtract A and a value, in BCD if D set                      |
| `sbc`       | 070            | All, w/ 8-bit immediate  | C, V, Z, S     | Subtract A and a value minus C, in BCD if D set              |
| `add`       | 100            | All, w/ 8-bit immediate  | C, V, Z, S     | Add A and a value, in BCD if D set                           |
| `adc`       | 110            | All, w/ 8-bit immediate  | C, V, Z, S     | Add A and a value plus C, in BCD if D set                    |
| `bit`       | 120            | All, w/ 8-bit immediate  | Z, S           | AND A and a value, discard the result but not the flags      |
| `and`       | 130            | All, w/ 8-bit immediate  | Z, S           | AND A and a value                                            |
| `or`        | 140            | All, w/ 8-bit immediate  | Z, S           | OR A and a value                                             |
| `xor`       | 150            | All, w/ 8-bit immediate  | Z, S           | XOR A and a value                                            |
| `jmp`       | 160            | All, w/ 8-bit immediate  |                | Jump to an address                                           |
| `call`      | 170            | All, w/ 8-bit immediate  |                | Push PC and jump to an address                               |
| `inc`       | 200            | All, w/ implied          | C, V, Z, S     | Increment a value                                            |
| `dec`       | 210            | All, w/ implied          | C, V, Z, S     | Decrement a value                                            |
| `shl`       | 220            | All, w/ implied          | C, Z, S        | Shift left a value                                           |
| `shr`       | 230            | All, w/ implied          | C, Z, S        | Shift right a value                                          |
| `sar`       | 240            | All, w/ implied          | C, Z, S        | Shift right a value, not changing sign bit                   |
| `not`       | 250            | All, w/ implied          | Z, S           | NOT a value                                                  |
| `rlc`       | 260            | All, w/ implied          | C, Z, S        | Rotate a value left through C                                |
| `rrc`       | 270            | All, w/ implied          | C, Z, S        | Rotate a value right through C                               |
| `jnc`       | 300            | Branch                   |                | Jump if C clear                                              |
| `jnv`       | 302            | Branch                   |                | Jump if V clear                                              |
| `jnz`       | 304            | Branch                   |                | Jump if Z clear                                              |
| `jns`       | 306            | Branch                   |                | Jump if S clear                                              |
| `jc`        | 310            | Branch                   |                | Jump if C set                                                |
| `jv`        | 312            | Branch                   |                | Jump if V set                                                |
| `jz`        | 314            | Branch                   |                | Jump if Z set                                                |
| `js`        | 316            | Branch                   |                | Jump if S set                                                |
| `clc`       | 320            | None                     | C              | Clear C                                                      |
| `cli`       | 321            | None                     | I              | Clear I                                                      |
| `clv`       | 322            | None                     | V              | Clear V                                                      |
| `cld`       | 323            | None                     | D              | Clear D                                                      |
| `clz`       | 324            | None                     | Z              | Clear Z                                                      |
| `cla`       | 325            | None                     |                | Clear A                                                      |
| `cls`       | 326            | None                     | S              | Clear S                                                      |
| `clx`       | 327            | None                     |                | Clear X                                                      |
| `stc`       | 330            | None                     | C              | Set C                                                        |
| `sti`       | 331            | None                     | I              | Set I                                                        |
| `stv`       | 332            | None                     | V              | Set V                                                        |
| `std`       | 333            | None                     | D              | Set D                                                        |
| `stz`       | 334            | None                     | Z              | Set Z                                                        |
| `ldsx`      | 335            | None                     | Z, S           | Load A from memory at X, increment X                         |
| `sts`       | 336            | None                     | S              | Set S                                                        |
| `stsx`      | 337            | None                     |                | Store A in memory at X, increment X                          |
| `ret`       | 340            | None                     |                | Pop PC from the stack                                        |
| `iret`      | 341            | None                     |                | Pop PSW and PC from the stack                                |
| `inx`       | 342            | None                     |                | Increment X                                                  |
| `dex`       | 343            | None                     |                | Decrement X                                                  |
| `txs`       | 344            | None                     |                | Move X into SP                                               |
| `tsx`       | 345            | None                     |                | Move SP into X                                               |
| `brk`       | 346            | None                     |                | Raise an interrupt                                           |
| `wai`       | 347            | None                     |                | Wait for an interrupt                                        |
| `php`       | 350            | None                     |                | Push PSW                                                     |
| `phx`       | 351            | None                     |                | Push X                                                       |
| `txal`      | 352            | None                     |                | Move A into the low byte of X                                |
| `txah`      | 353            | None                     |                | Move A into the high byte of X                               |
| `plp`       | 354            | None                     |                | Pop PSW                                                      |
| `plx`       | 355            | None                     |                | Pop X                                                        |
| `taxl`      | 356            | None                     |                | Move the low byte of X into A                                |
| `taxh`      | 357            | None                     |                | Move the high byte of X into A                               |
| `cxz`       | 360            | None                     | Z, S           | Compare X to zero                                            |

Additionally, `nop` is opcode 161, so immediate `jmp`. This works
because the jump directly sets PC to the address of the immediate, so
it is not actually incremented.

# PPU

The PPU (Picture Processing Unit) is the unit responsible for video
output. It produces a visible output of 232x176 pixels in 76800
(300&times;256) clock cycles, giving an effective refresh rate of 60
Hz.

There is a background layer of 32x32 tiles, and up to 32 sprites (8x8
or 8x16). The tiles are 8x8 and have 4 colors, inside 4 palettes for
background and sprites each. There can only be 8 sprites per scanline.

The background is smoothly scrollable.

These "virtual frames" of 300 columns by 256 lines are divided as
such:

- Lines 0-79 - Idle ("vertical blanking")
- Lines 80-255 - Generating a picture
  - Columns 0-31 - Searching for active sprites
  - Column 32-56 (by 4) - Generate active sprites
  - Columns 60-292 (by 8) - Generating background and blitting sprites

The PPU doesn't use interrupts, but instead synchronously halts the
processor until a specified line is reached.

## Tiles

Tiles are stored in character RAM, 256 tiles being addressable at a
time. Each tile is 16 bytes, with each pair of bytes being one line of
the tile. The first byte is the color low bit of each pixel, and the
second the color high byte.

## Map and attribute RAM

Background data is stored in the 1 KiB map RAM and the 512-byte
attribute RAM. The map RAM stores a 32x32 array of tiles, of which
24x20 to 25x21 is actually displayed at any given moment.

The attribute RAM stores 4 bits for each tile in the background:

- Bits 0-1 - Palette index
- Bit 2 - Horizontal flip
- Bit 3 - Vertical flip

## Sprites

Sprites are stored in the 256-byte sprite RAM. Each sprite is 4 bytes:
the X position, Y position, flags, and tile index.

The flags byte has the structure:

- Bits 0-1 - Palette index
- Bit 2 - 16-line sprite
- Bit 3 - Above (0) or behind (1) background layer
- Bit 4 - Horizontal flip
- Bit 5 - Vertical flip
- Bit 7 - Visible

## Palettes

There are 8 palettes available, 4 for background and 4 for
sprites. The palettes are stored in the 32-byte palette RAM, each
palette being 4 bytes (1 byte per index inside each palette). Palettes
0-3 are the background ones, and 4-7 are the sprites one.

## Color

The PPU supports 128 colors, represented in a byte formatted like this:

- Bit 0-1 - Value
- Bit 2-3 - Saturation
- Bit 4-6 - Hue
- Bit 7 - Transparent

For S != 0, it is the HSV color represented by `hue * 45deg`,
`saturation / 3`, `(value + 1) / 4`. For S = 0, then it is the
grayscale value `hue * 4 + value`.

## IO ports

- 0x4040 - `PPU_WAIT` - RW - Halt processor until this line is reached
- 0x4042 - `PPU_SCROLLX` - RW - Background X scroll
- 0x4043 - `PPU_SCROLLY` - RW - Background Y scroll

# AGU

The AGU (Audio Generation Unit) is the unit that handles audio
generation, unsurprisingly.

It plays samples of up to 512 entries of 4-bit (offset by 8) PCM
through 4 channels at a sample rate of 12000 (master clock divided by
384).

The samples are stored in the 256-byte audio RAM in a raw
least-significant-bit-first format.

## IO ports

- 0x4020-0x402C - `AGU_C0BASE`-`AGU_C3BASE` - W - Base address within audio RAM
- 0x4021-0x402D - `AGU_C0LEN`-`AGU_C3LEN` - W - Number of samples to play
- 0x4022-0x402E - `AGU_C0SPEED`-`AGU_C3SPEED` - W - 4.4 unsigned fixed-point playback speed
- 0x4023-0x402F - `AGU_C0VOL`-`AGU_C3VOL` - W
  - Bits 0-3 - 1.3 unsigned fixed-point volume
  - Bit 6 - Reset bit index

# ROM

The FEMTO-8 ROM file format is made of a 16-byte header, followed by
the program data.

ROM is mapped at 0x8000 and made of up to 16 8 KiB banks, with 4
separate banks mapped in memory at any time, giving a maximum size of 128 KiB.

The header has the following structure:

- Bytes 0-7 - Magic number (`FEMTO-8` and NUL in ASCII)
- Byte 8 - Flags
  - Bits 0-2 - Size shift (0x2000 << n)
- Bytes 9-15 - Reserved
