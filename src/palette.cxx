#include <cmath>
#include <cstdint>
#include <vector>
#include "palette.hxx"

std::vector<uint32_t> palette;

void init_palette() {
    palette = std::vector<uint32_t>(128);

    for (auto i = 0; i < 128; i++) {
        auto ih = i >> 4 & 15;
        auto is = i >> 2 & 3;
        auto iv = i >> 0 & 3;

        double r, g, b;

        if (is == 0) {
            r = g = b = (ih * 4 + iv) / 31.;
        } else {
            auto h = ih / 8.;
            auto s = is / 3.;
            auto v = (iv + 1) / 4.;

            auto hi = floor(h * 6.);
            auto f = h * 6. - hi;
            auto p = v * (1. - s);
            auto q = v * (1. - f * s);
            auto t = v * (1. - (1. - f) * s);

            switch (static_cast<int>(hi) % 6) {
            case 0: r = v; g = t; b = p; break;
            case 1: r = q; g = v; b = p; break;
            case 2: r = p; g = v; b = t; break;
            case 3: r = p; g = q; b = v; break;
            case 4: r = t; g = p; b = v; break;
            case 5: r = v; g = p; b = q; break;
            }
        }

        auto ir = static_cast<int>(round(r * 255));
        auto ig = static_cast<int>(round(g * 255));
        auto ib = static_cast<int>(round(b * 255));
        palette[i] = 0x000000FF | ir << 24 | ig << 16 | ib << 8;
    }
}
