#pragma once

template<auto F>
struct deleter {
    template<typename T>
    void operator()(T* ptr) {
        F(ptr);
    }
};
