#pragma once

#include <cstdint>
#include <vector>

struct agu_t {
    struct channel_t {
        unsigned base, len;
        int16_t i, speed, vol;


        void reset() noexcept;
        int32_t cycle(const uint8_t* arr) noexcept;
    };

    struct system_t* system;

    std::vector<uint8_t> audio_ram;
    channel_t channels[4];

    agu_t(struct system_t* system);

    void reset() noexcept;
    void write(uint16_t reg, uint8_t data) noexcept;
    void read(uint16_t reg) noexcept;

    void cycle() noexcept;
};
