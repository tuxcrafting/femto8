#include <memory>
#include <string>
#include <unordered_map>
#include "cartridge.hxx"
#include "frontend.hxx"

struct main_t {
    std::unordered_map<std::string, std::string> options;
    std::unique_ptr<frontend_t> fe;
    cartridge_t cart;

    void do_cart();
    void do_scale();
    void do_keys();
    void main();
};
