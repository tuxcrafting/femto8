#include <SDL2/SDL.h>
#include <cmath>
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include "cartridge.hxx"
#include "frontend.hxx"
#include "palette.hxx"
#include "system.hxx"
#include "system_defs.hxx"

sdl_error_t::sdl_error_t(const std::string& fn)
    : std::runtime_error(fn + " error: " + std::string(SDL_GetError())) { }

static void audio_cb(void* userdata, uint8_t* stream, int len) {
    auto self = static_cast<frontend_t*>(userdata);
    auto streamf = reinterpret_cast<float*>(stream);
    for (auto i = 0; i < len / 4; i++)
        streamf[i] = self->system.get_audio();
}

frontend_t::frontend_t() {
    window.reset(
        SDL_CreateWindow(
            "FEMTO-8",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            FB_WIDTH, FB_HEIGHT,
            SDL_WINDOW_RESIZABLE));
    if (window.get() == nullptr)
        throw sdl_error_t("SDL_CreateWindow");

    renderer.reset(
        SDL_CreateRenderer(
            window.get(), -1,
            SDL_RENDERER_PRESENTVSYNC));
    if (renderer.get() == nullptr)
        throw sdl_error_t("SDL_CreateRenderer");

    fbtex.reset(
        SDL_CreateTexture(
            renderer.get(),
            SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_STREAMING,
            FB_WIDTH, FB_HEIGHT));
    if (fbtex.get() == nullptr)
        throw sdl_error_t("SDL_CreateTexture");

    SDL_AudioSpec have, want = {
        .freq = 48000,
        .format = AUDIO_F32SYS,
        .channels = 1,
        .samples = 4096,
        .callback = &audio_cb,
        .userdata = this,
    };
    audiodev = SDL_OpenAudioDevice(nullptr, 0, &want, &have, 0);
    if (audiodev == 0)
        throw sdl_error_t("SDL_OpenAudioDevice");

    SDL_PauseAudioDevice(audiodev, 0);
}

void frontend_t::resize(unsigned w, unsigned h) {
    SDL_SetWindowSize(window.get(), w, h);
}

void frontend_t::do_key(SDL_Keycode key, bool down) {
    auto k = keymap.find(key);
    if (k == keymap.end())
        return;

    uint32_t mask = std::get<1>(*k);
    if (down)
        system.ctrl |= mask;
    else
        system.ctrl &= ~mask;
}

void frontend_t::handle(const SDL_Event& ev) {
    switch (ev.type) {
    case SDL_QUIT:
        running = false;
        break;
    case SDL_KEYDOWN:
        do_key(ev.key.keysym.sym, true);
        break;
    case SDL_KEYUP:
        do_key(ev.key.keysym.sym, false);
        break;
    }
}

void frontend_t::frame(int delta) {
    system.run(delta * (CLOCK / 1000));

    void* pixels;
    int pitch;
    SDL_LockTexture(fbtex.get(), nullptr, &pixels, &pitch);
    auto pix32 = static_cast<uint32_t*>(pixels);
    for (auto y = 0; y < FB_HEIGHT; y++) {
        for (auto x = 0; x < FB_WIDTH; x++) {
            auto i = y * FB_WIDTH + x;
            auto j = (y * pitch + x * 4) / 4;
            pix32[j] = palette[system.video_out[i] & 0x7F];
        }
    }
    SDL_UnlockTexture(fbtex.get());

    SDL_RenderClear(renderer.get());
    SDL_RenderCopy(renderer.get(), fbtex.get(), nullptr, nullptr);
    SDL_RenderPresent(renderer.get());
}

void frontend_t::mainloop() {
    running = true;
    auto t1 = SDL_GetTicks64();
    while (running) {
        SDL_Event ev;
        while (SDL_PollEvent(&ev))
            handle(ev);
        auto t2 = SDL_GetTicks64();
        frame(t2 - t1);
        t1 = t2;
    }
}
