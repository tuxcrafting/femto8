#pragma once

constexpr auto RAM_SIZE = 0x2000;
constexpr auto RAM_MASK = RAM_SIZE - 1;

constexpr auto CHR_RAM_SIZE = 0x1000;
constexpr auto CHR_RAM_MASK = CHR_RAM_SIZE - 1;

constexpr auto MAP_RAM_SIZE = 0x400;
constexpr auto MAP_RAM_MASK = MAP_RAM_SIZE - 1;

constexpr auto SPRITE_RAM_SIZE = 0x100;
constexpr auto SPRITE_RAM_MASK = SPRITE_RAM_SIZE - 1;

constexpr auto ATTR_RAM_SIZE = 0x200;
constexpr auto ATTR_RAM_MASK = ATTR_RAM_SIZE - 1;

constexpr auto PAL_RAM_SIZE = 0x20;
constexpr auto PAL_RAM_MASK = PAL_RAM_SIZE - 1;

constexpr auto AUDIO_RAM_SIZE = 0x100;
constexpr auto AUDIO_RAM_MASK = AUDIO_RAM_SIZE - 1;

constexpr auto CLOCK = 4'608'000;
constexpr auto CPU_DIV = 2;
constexpr auto AGU_DIV = 384;

constexpr auto AUDIO_QUEUE_SIZE = 8192;
constexpr auto AUDIO_QUEUE_MASK = AUDIO_QUEUE_SIZE - 1;

constexpr auto FB_WIDTH = 232;
constexpr auto FB_HEIGHT = 176;

constexpr auto SYS_MAP01 = 0x00;
constexpr auto SYS_MAP23 = 0x01;

constexpr auto SYS_SERIAL_OUT = 0x04;

constexpr auto SYS_CTRL0 = 0x08;
constexpr auto SYS_CTRL1 = 0x09;
constexpr auto SYS_CTRL2 = 0x0A;
constexpr auto SYS_CTRL3 = 0x0B;

constexpr auto AGU_C0BASE = 0x00;
constexpr auto AGU_C0LEN = 0x01;
constexpr auto AGU_C0SPEED = 0x02;
constexpr auto AGU_C0VOL = 0x03;

constexpr auto AGU_C1BASE = 0x04;
constexpr auto AGU_C1LEN = 0x05;
constexpr auto AGU_C1SPEED = 0x06;
constexpr auto AGU_C1VOL = 0x07;

constexpr auto AGU_C2BASE = 0x08;
constexpr auto AGU_C2LEN = 0x09;
constexpr auto AGU_C2SPEED = 0x0A;
constexpr auto AGU_C2VOL = 0x0B;

constexpr auto AGU_C3BASE = 0x0C;
constexpr auto AGU_C3LEN = 0x0D;
constexpr auto AGU_C3SPEED = 0x0E;
constexpr auto AGU_C3VOL = 0x0F;

constexpr auto PPU_WAIT = 0x00;
constexpr auto PPU_SCROLLX = 0x02;
constexpr auto PPU_SCROLLY = 0x03;
