#include <cstdint>
#include "cpu.hxx"
#include "cpu_opcodes.hxx"
#include "system.hxx"

cpu_t::cpu_t(struct system_t* system) noexcept
    : system(system) { }

void cpu_t::write8(uint16_t addr, uint8_t data) noexcept {
    system->advance();
    system->write8(addr, data);
}

void cpu_t::write16(uint16_t addr, uint16_t data) noexcept {
    write8(addr + 0, data >> 0);
    write8(addr + 1, data >> 8);
}

uint8_t cpu_t::read8(uint16_t addr) noexcept {
    system->advance();
    return system->read8(addr);
}

uint16_t cpu_t::read16(uint16_t addr) noexcept {
    return read8(addr + 0) << 0 | read8(addr + 1) << 8;
}

uint8_t cpu_t::fetch8() noexcept {
    return read8(pc++);
}

uint16_t cpu_t::fetch16() noexcept {
    return read16((pc += 2) - 2);
}

void cpu_t::push8(uint8_t data) noexcept {
    write8(--sp, data);
}

void cpu_t::push16(uint16_t data) noexcept {
    write16(sp -= 2, data);
}

uint8_t cpu_t::pop8() noexcept {
    return read8(sp++);
}

uint16_t cpu_t::pop16() noexcept {
    return read16((sp += 2) - 2);
}

uint8_t cpu_t::getf() const noexcept {
    return 0xC0 |
        f_c << 0 |
        f_v << 1 |
        f_z << 2 |
        f_s << 3 |
        f_i << 4 |
        f_d << 5;
}

void cpu_t::setf(uint8_t x) noexcept {
    f_c = x >> 0 & 1;
    f_v = x >> 1 & 1;
    f_z = x >> 2 & 1;
    f_s = x >> 3 & 1;
    f_i = x >> 4 & 1;
    f_d = x >> 5 & 1;
}

uint8_t cpu_t::adc(uint8_t x, uint8_t y, bool cin, bool bcd) noexcept {
    uint16_t r = x + y + cin;
    uint16_t rt = (x & 0x7F) + (y & 0x7F) + cin;

    if (bcd) {
        if ((r & 0x0F) > 0x09) r += 0x06;
        if ((r & 0xF0) > 0x90) r += 0x60;
    }

    f_c = r >> 8 & 1;
    f_v = (r >> 8 & 1) ^ (rt >> 7 & 1);
    return setzs(r);
}

uint8_t cpu_t::setzs(uint8_t x) noexcept {
    f_z = x == 0;
    f_s = x >> 7 & 1;
    return x;
}

uint16_t cpu_t::addr_imm_8() noexcept { return pc++; }
uint16_t cpu_t::addr_imm_16() noexcept { return (pc += 2) - 2; }

uint16_t cpu_t::addr_abs_8() noexcept { return fetch8(); }
uint16_t cpu_t::addr_abs_8s() noexcept { auto t = fetch8(); return (pc & 0xFF00) | t; }
uint16_t cpu_t::addr_abs_16() noexcept { return fetch16(); }

uint16_t cpu_t::addr_ind_8() noexcept { return read16(fetch8()); }
uint16_t cpu_t::addr_ind_16() noexcept { return read16(fetch16()); }

uint16_t cpu_t::addr_idx() noexcept { return fetch16() + index; }
uint16_t cpu_t::addr_ind_idx() noexcept { return read16(fetch8()) + index; }
uint16_t cpu_t::addr_idx_ind() noexcept { return read16(fetch16() + index); }

void cpu_t::op_ld(uint16_t addr) noexcept { acc = setzs(read8(addr)); }
void cpu_t::op_st(uint16_t addr) noexcept { write8(addr, acc); }

void cpu_t::op_ldx(uint16_t addr) noexcept { index = read16(addr); }
void cpu_t::op_stx(uint16_t addr) noexcept { write16(addr, index); }
void cpu_t::op_ldxa(uint16_t addr) noexcept { index = addr; }

void cpu_t::op_cmp(uint16_t addr) noexcept { adc(acc, ~read8(addr), true, false); }
void cpu_t::op_sub(uint16_t addr) noexcept { acc = adc(acc, ~read8(addr), true, f_d); }
void cpu_t::op_sbc(uint16_t addr) noexcept { acc = adc(acc, ~read8(addr), f_c, f_d); }

void cpu_t::op_add(uint16_t addr) noexcept { acc = adc(acc, read8(addr), false, f_d); }
void cpu_t::op_adc(uint16_t addr) noexcept { acc = adc(acc, read8(addr), f_c, f_d); }

void cpu_t::op_bit(uint16_t addr) noexcept { setzs(acc & read8(addr)); }

void cpu_t::op_and(uint16_t addr) noexcept { acc = setzs(acc & read8(addr)); }
void cpu_t::op_or(uint16_t addr) noexcept { acc = setzs(acc | read8(addr)); }
void cpu_t::op_xor(uint16_t addr) noexcept { acc = setzs(acc ^ read8(addr)); }

void cpu_t::op_jmp(uint16_t addr) noexcept { pc = addr; }
void cpu_t::op_call(uint16_t addr) noexcept { push16(pc); pc = addr; }

uint8_t cpu_t::op_inc(uint8_t data) noexcept { return adc(data, 1, false, false); }
uint8_t cpu_t::op_dec(uint8_t data) noexcept { return adc(data, -1, false, false); }

uint8_t cpu_t::op_shl(uint8_t data) noexcept { f_c = data >> 7 & 1; return setzs(data << 1); }
uint8_t cpu_t::op_shr(uint8_t data) noexcept { f_c = data >> 0 & 1; return setzs(data >> 1); }
uint8_t cpu_t::op_sar(uint8_t data) noexcept { f_c = data >> 0 & 1; return setzs(data >> 1 | (data & 0x80)); }

uint8_t cpu_t::op_not(uint8_t data) noexcept { return setzs(~data); }

uint8_t cpu_t::op_rlc(uint8_t data) noexcept { auto t = data << 1 | f_c << 0; f_c = data >> 7 & 1; return t; }
uint8_t cpu_t::op_rrc(uint8_t data) noexcept { auto t = data >> 1 | f_c << 7; f_c = data >> 0 & 1; return t; }

void cpu_t::reset() noexcept {
    acc = 0x00;
    index = 0x0000;
    sp = 0x0200;
    pc = 0xFF00;

    f_c = false;
    f_v = false;
    f_z = false;
    f_s = false;
    f_i = false;
    f_d = false;

    waiting = false;
}

#define ADDR_MODE(op_name, addr_name) \
    case OP_ ## op_name + ADDR_ ## addr_name: op_ ## op_name(addr_ ## addr_name()); break

#define RMW_MODE(op_name, addr_name) \
    case OP_ ## op_name + ADDR_ ## addr_name: { auto t = addr_ ## addr_name(); write8(t, op_ ## op_name(read8(t))); } break

#define OP_F_N(f, op_name) \
    f(op_name, abs_16); \
    f(op_name, ind_8); \
    f(op_name, ind_16); \
    f(op_name, idx); \
    f(op_name, ind_idx); \
    f(op_name, idx_ind)

#define OP_ADDR_8(op_name) \
    OP_F_N(ADDR_MODE, op_name); \
    ADDR_MODE(op_name, abs_8); \
    ADDR_MODE(op_name, imm_8)
#define OP_ADDR_16(op_name) \
    OP_F_N(ADDR_MODE, op_name); \
    ADDR_MODE(op_name, abs_8); \
    ADDR_MODE(op_name, imm_16)
#define OP_ADDR_8S(op_name) \
    OP_F_N(ADDR_MODE, op_name); \
    ADDR_MODE(op_name, abs_8s); \
    ADDR_MODE(op_name, imm_8)

#define OP_RMW(op_name) \
    OP_F_N(RMW_MODE, op_name); \
    RMW_MODE(op_name, abs_8); \
    case OP_ ## op_name + ADDR_imp: acc = op_ ## op_name(acc); break

#define OP_BRANCH(op_name, cond) \
    case OP_ ## op_name + 0: { auto t = fetch8(); if (cond) pc = (pc & 0xFF00) | t; }; break; \
    case OP_ ## op_name + 1: { auto t = fetch16(); if (cond) pc = t; }; break; \

void cpu_t::step() noexcept {
    uint8_t opcode;
    if (f_i && system->raise) {
        waiting = false;
        opcode = OP_brk;
    } else if (waiting) {
        system->advance();
        return;
    } else {
        opcode = fetch8();
    }

    switch (opcode) {

        OP_ADDR_8(ld);
        OP_ADDR_8(st);

        OP_ADDR_16(ldx);
        OP_ADDR_16(stx);
        OP_ADDR_8(ldxa);

        OP_ADDR_8(cmp);
        OP_ADDR_8(sub);
        OP_ADDR_8(sbc);

        OP_ADDR_8(add);
        OP_ADDR_8(adc);

        OP_ADDR_8(bit);

        OP_ADDR_8(and);
        OP_ADDR_8(or);
        OP_ADDR_8(xor);

        OP_ADDR_8S(jmp);
        OP_ADDR_8(call);

        OP_RMW(inc);
        OP_RMW(dec);

        OP_RMW(shl);
        OP_RMW(shr);
        OP_RMW(sar);

        OP_RMW(not);

        OP_RMW(rlc);
        OP_RMW(rrc);

        OP_BRANCH(jnc, !f_c);
        OP_BRANCH(jnv, !f_v);
        OP_BRANCH(jnz, !f_z);
        OP_BRANCH(jns, !f_s);

        OP_BRANCH(jc, f_c);
        OP_BRANCH(jv, f_v);
        OP_BRANCH(jz, f_z);
        OP_BRANCH(js, f_s);

    case OP_clc: f_c = false; break;
    case OP_cli: f_i = false; break;
    case OP_clv: f_v = false; break;
    case OP_cld: f_d = false; break;
    case OP_clz: f_z = false; break;
    case OP_clx: index = 0; break;
    case OP_cls: f_s = false; break;
    case OP_cla: acc = 0; break;

    case OP_stc: f_c = true; break;
    case OP_sti: f_i = true; break;
    case OP_stv: f_v = true; break;
    case OP_std: f_d = true; break;
    case OP_stz: f_z = true; break;
    case OP_ldsx: acc = setzs(read8(index++)); break;
    case OP_sts: f_s = true; break;
    case OP_stsx: write8(index++, acc); break;

    case OP_ret: pc = pop16(); break;
    case OP_iret:
        setf(pop8());
        acc = pop8();
        pc = pop16();
        break;

    case OP_inx: index++; break;
    case OP_dex: index--; break;

    case OP_txs: sp = index; break;
    case OP_tsx: index = sp; break;

    case OP_brk:
        push16(pc);
        push8(acc);
        push8(getf());
        f_i = false;
        f_d = false;
        pc = 0xFF03;
        break;
    case OP_wai: waiting = true; break;

    case OP_php: push8(acc); push8(getf()); break;
    case OP_phx: push16(index); break;

    case OP_txal: acc = index >> 0 & 0xFF; break;
    case OP_txah: acc = index >> 8 & 0xFF; break;

    case OP_plp: setf(pop8()); acc = pop8(); break;
    case OP_plx: index = pop16(); break;

    case OP_taxl: index = (index & 0xFF00) | acc << 0; break;
    case OP_taxh: index = (index & 0x00FF) | acc << 8; break;

    case OP_cxz:
        f_z = index == 0;
        f_s = index >> 15 & 1;
        break;

    }
}
