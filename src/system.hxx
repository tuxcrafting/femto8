#pragma once

#include <cstdint>
#include <vector>
#include "agu.hxx"
#include "cartridge.hxx"
#include "cpu.hxx"
#include "ppu.hxx"

struct system_t {
    cartridge_t* cart;

    cpu_t cpu;
    ppu_t ppu;
    agu_t agu;
    uint8_t open_bus;
    bool raise, halt;

    std::vector<float> audio;
    std::vector<uint8_t> video_tmp;
    std::vector<uint8_t> video_out;
    std::vector<uint8_t> ram;

    unsigned mapper[4];
    uint32_t ctrl;

    uint64_t cyc_count;
    uint64_t cyc_lc;
    uint64_t cyc_to;
    unsigned audio_i, audio_j;

    system_t();

    void set_cart(cartridge_t* cart) noexcept;
    void reset() noexcept;
    void advance() noexcept;

    uint8_t read_prg(uint16_t addr) const noexcept;

    void write8(uint16_t addr, uint8_t data) noexcept;
    uint8_t read8(uint16_t addr) noexcept;

    void put_video(uint8_t x, uint8_t y, uint8_t p) noexcept;

    void put_audio(float x) noexcept;
    float get_audio() noexcept;

    void nocpu_cycle() noexcept;
    void catch_up() noexcept;
    void step() noexcept;
    void run(int n) noexcept;
};
