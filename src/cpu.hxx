#pragma once

#include <cstdint>

struct cpu_t {
    struct system_t* system;

    uint8_t acc;
    uint16_t index;
    uint16_t sp;
    uint16_t pc;

    bool f_c;
    bool f_v;
    bool f_z;
    bool f_s;
    bool f_i;
    bool f_d;

    bool waiting;

    cpu_t(struct system_t* system) noexcept;

    void write8(uint16_t addr, uint8_t data) noexcept;
    void write16(uint16_t addr, uint16_t data) noexcept;

    uint8_t read8(uint16_t addr) noexcept;
    uint16_t read16(uint16_t addr) noexcept;

    uint8_t fetch8() noexcept;
    uint16_t fetch16() noexcept;

    void push8(uint8_t data) noexcept;
    void push16(uint16_t data) noexcept;

    uint8_t pop8() noexcept;
    uint16_t pop16() noexcept;

    uint8_t getf() const noexcept;
    void setf(uint8_t x) noexcept;

    uint8_t adc(uint8_t x, uint8_t y, bool cin, bool bcd) noexcept;
    uint8_t setzs(uint8_t x) noexcept;

    uint16_t addr_imm_8() noexcept;
    uint16_t addr_imm_16() noexcept;

    uint16_t addr_abs_8() noexcept;
    uint16_t addr_abs_8s() noexcept;
    uint16_t addr_abs_16() noexcept;

    uint16_t addr_ind_8() noexcept;
    uint16_t addr_ind_16() noexcept;

    uint16_t addr_idx() noexcept;
    uint16_t addr_ind_idx() noexcept;
    uint16_t addr_idx_ind() noexcept;

    void op_ld(uint16_t addr) noexcept;
    void op_st(uint16_t addr) noexcept;

    void op_ldx(uint16_t addr) noexcept;
    void op_stx(uint16_t addr) noexcept;
    void op_ldxa(uint16_t addr) noexcept;

    void op_cmp(uint16_t addr) noexcept;
    void op_sub(uint16_t addr) noexcept;
    void op_sbc(uint16_t addr) noexcept;

    void op_add(uint16_t addr) noexcept;
    void op_adc(uint16_t addr) noexcept;

    void op_bit(uint16_t addr) noexcept;

    void op_and(uint16_t addr) noexcept;
    void op_or(uint16_t addr) noexcept;
    void op_xor(uint16_t addr) noexcept;

    void op_jmp(uint16_t addr) noexcept;
    void op_call(uint16_t addr) noexcept;

    uint8_t op_inc(uint8_t data) noexcept;
    uint8_t op_dec(uint8_t data) noexcept;

    uint8_t op_shl(uint8_t data) noexcept;
    uint8_t op_shr(uint8_t data) noexcept;
    uint8_t op_sar(uint8_t data) noexcept;

    uint8_t op_not(uint8_t data) noexcept;

    uint8_t op_rlc(uint8_t data) noexcept;
    uint8_t op_rrc(uint8_t data) noexcept;

    void reset() noexcept;
    void step() noexcept;
};
