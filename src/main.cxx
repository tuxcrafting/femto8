#include <SDL2/SDL.h>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>
#include "cartridge.hxx"
#include "frontend.hxx"
#include "main.hxx"
#include "palette.hxx"
#include "system_defs.hxx"

void main_t::do_cart() {
    auto rom_opt = options.find("rom");
    if (rom_opt == options.end())
        throw std::runtime_error("expected 'rom' option");

    std::ifstream rom_file(std::get<1>(*rom_opt), std::ios::in | std::ios::binary);
    std::vector<uint8_t> rom_data;

    char c;
    while (!rom_file.get(c).fail())
        rom_data.push_back(c);

    if (!rom_file.eof())
        throw std::runtime_error("failed reading ROM file");

    cart.load(rom_data.size(), &rom_data[0]);
}

void main_t::do_scale() {
    auto scale_opt = options.find("scale");
    if (scale_opt == options.end()) return;

    auto scale = std::atoi(std::get<1>(*scale_opt).c_str());
    fe->resize(FB_WIDTH * scale, FB_HEIGHT * scale);
}

static std::unordered_map<std::string, int> ctrl_keys = {
    { "Left0", 0 }, { "Up0", 1 }, { "Right0", 2 }, { "Down0", 3 },
    { "Start0", 4 }, { "A0", 5 }, { "B0", 6 }, { "C0", 7 },

    { "Left1", 8 }, { "Up1", 9 }, { "Right1", 10 }, { "Down1", 11 },
    { "Start1", 12 }, { "A1", 13 }, { "B1", 14 }, { "C1", 15 },

    { "Left2", 16 }, { "Up2", 17 }, { "Right2", 18 }, { "Down2", 19 },
    { "Start2", 20 }, { "A2", 21 }, { "B2", 22 }, { "C2", 23 },

    { "Left3", 24 }, { "Up3", 25 }, { "Right3", 26 }, { "Down3", 27 },
    { "Start3", 28 }, { "A3", 29 }, { "B3", 30 }, { "C3", 31 },
};

void main_t::do_keys() {
    auto keys_opt = options.find("keys");
    if (keys_opt == options.end()) return;

    auto keys = std::get<1>(*keys_opt);
    size_t pos = 0;

    std::string buf, key;
    bool esc = false;

    for (;;) {
        auto c = pos != keys.size() ? keys[pos++] : -1;

        if (esc) {
            buf += c;
            esc = false;
        } else {
            switch (c) {
            case -1:
            case ';': {
                if (key.size() == 0)
                    break;

                auto key_n = SDL_GetKeyFromName(key.c_str());
                if (key_n == SDLK_UNKNOWN)
                    throw std::runtime_error("invalid keyboard key name " + key);

                auto val_t = ctrl_keys.find(buf);
                if (val_t == ctrl_keys.end())
                    throw std::runtime_error("invalid controller key name " + buf);
                fe->keymap.insert(std::make_pair(key_n, 1u << std::get<1>(*val_t)));

                key = "";
                buf = "";
                break;
            }
            case ' ':
                break;
            case ':':
                key = buf;
                buf = "";
                break;
            case '\\':
                esc = true;
                break;
            default:
                buf += c;
            }
        }

        if (c == -1)
            break;
    }
}

void main_t::main() {
    init_palette();

    do_cart();

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
        throw sdl_error_t("SDL_Init");

    fe = std::make_unique<frontend_t>();

    do_scale();
    do_keys();

    fe->system.set_cart(&cart);
    fe->system.reset();
    fe->mainloop();

    SDL_Quit();
}

int main(int argc, const char** argv) {
    main_t main_;

    for (auto i = 1; i < argc; i += 2) {
        std::string key(argv[i + 0]);
        std::string value(argv[i + 1]);
        auto t = main_.options.find(key);
        if (t == main_.options.end())
            main_.options.insert(std::pair(key, value));
        else
            std::get<1>(*t) += value;
    }

    try {
        main_.main();
    } catch (std::exception& ex) {
        std::fprintf(stderr, "exception: %s\n", ex.what());
        std::exit(EXIT_FAILURE);
    }
    return EXIT_SUCCESS;
}
