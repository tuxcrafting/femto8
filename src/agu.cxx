#include <algorithm>
#include <cmath>
#include <cstdint>
#include <vector>
#include "agu.hxx"
#include "system.hxx"
#include "system_defs.hxx"

void agu_t::channel_t::reset() noexcept {
    base = len = 0;
    i = speed = vol = 0;
}

int32_t agu_t::channel_t::cycle(const uint8_t* arr) noexcept {
    auto ii = i >> 8 & 0xFF;
    int32_t out = (((arr[base + (ii >> 1) & 0xFF] >> 4 * (ii & 1)) & 15) << 5) - 0x100;

    i += speed;
    if (i >= (len + 1) << 8)
        i -= (len + 1) << 8;
    return out * vol;
}

agu_t::agu_t(system_t* system)
    : system(system)
    , audio_ram(AUDIO_RAM_SIZE) { }

void agu_t::reset() noexcept {
    std::fill(audio_ram.begin(), audio_ram.end(), 0);
    for (auto& ch : channels) ch.reset();
}

void agu_t::write(uint16_t reg, uint8_t data) noexcept {
    auto& ch = channels[reg >> 2 & 3];
    switch (reg & 3) {
    case AGU_C0BASE:
        ch.base = data;
        break;
    case AGU_C0LEN:
        ch.len = data >> 0 & 0xFF;
        break;
    case AGU_C0SPEED:
        ch.speed = (data >> 0 & 0xFF) << 4;
        break;
    case AGU_C0VOL:
        ch.vol = (data >> 0 & 15) << 5;
        if (data >> 6 & 1) ch.i = 0x0000;
        break;
    }
}

void agu_t::read(uint16_t reg) noexcept {
}

void agu_t::cycle() noexcept {
    int32_t x = 0;
    for (auto& ch : channels)
        x += ch.cycle(&audio_ram[0]);

    auto xf = static_cast<float>(x) / 65536.f;
    if (xf > 1.f) xf = 1.f;
    if (xf < -1.f) xf = -1.f;
    xf *= 0.25f;

    for (auto i = 0; i < 4; i++)
        system->put_audio(xf);
}
