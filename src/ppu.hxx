#pragma once

#include <cstdint>
#include <optional>
#include <vector>

struct ppu_t {
    struct sprite_t {
        uint8_t x, y;
        uint8_t pal : 2;
        uint8_t h16 : 1;
        uint8_t hi : 1;
        uint8_t flipx : 1;
        uint8_t flipy : 1;
        uint8_t _0 : 1;
        uint8_t visible : 1;
        uint8_t tile;
    };

    struct system_t* system;

    std::vector<uint8_t> chr_ram;
    std::vector<uint8_t> map_ram;
    std::vector<uint8_t> sprite_ram;
    std::vector<uint8_t> attr_ram;
    std::vector<uint8_t> pal_ram;
    std::vector<uint8_t> scanline;

    bool vsync;
    unsigned pix_x, pix_y;

    std::optional<uint8_t> wait;
    uint8_t scroll_x, scroll_y;

    unsigned sl_spr[8], sl_spr_i;

    ppu_t(struct system_t* system);
    void reset() noexcept;
    void write(uint16_t reg, uint8_t data) noexcept;
    void read(uint16_t reg) noexcept;

    const ppu_t::sprite_t* get_sprite(unsigned i) const noexcept;
    void draw_tile(uint8_t tile_i, uint8_t x, uint8_t fy, uint8_t pal, bool hi, bool flipx, bool flipy) noexcept;

    void sl_check_sprite(unsigned i) noexcept;
    void sl_draw_sprite(unsigned i) noexcept;
    void sl_draw_tile(unsigned px) noexcept;

    void cycle() noexcept;
};
