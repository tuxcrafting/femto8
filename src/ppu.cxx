#include <algorithm>
#include <cstdint>
#include <optional>
#include <vector>
#include "ppu.hxx"
#include "system.hxx"
#include "system_defs.hxx"

ppu_t::ppu_t(system_t* system)
    : system(system)
    , chr_ram(CHR_RAM_SIZE)
    , map_ram(MAP_RAM_SIZE)
    , sprite_ram(SPRITE_RAM_SIZE)
    , attr_ram(ATTR_RAM_SIZE)
    , pal_ram(PAL_RAM_SIZE)
    , scanline(256) { }

void ppu_t::reset() noexcept {
    pix_x = 0;
    pix_y = 0;
    vsync = false;

    wait = std::nullopt;
    scroll_x = 0x00;
    scroll_y = 0x00;

    std::fill(chr_ram.begin(), chr_ram.end(), 0x00);
    std::fill(map_ram.begin(), map_ram.end(), 0x00);
    std::fill(sprite_ram.begin(), sprite_ram.end(), 0x00);
    std::fill(attr_ram.begin(), attr_ram.end(), 0x00);
    std::fill(pal_ram.begin(), pal_ram.end(), 0xFF);
}

void ppu_t::write(uint16_t reg, uint8_t data) noexcept {
    switch (reg) {

    case PPU_WAIT:
        wait = std::make_optional(data);
        system->halt = true;
        break;

    case PPU_SCROLLX: scroll_x = data; break;
    case PPU_SCROLLY: scroll_y = data; break;

    }
}

void ppu_t::read(uint16_t reg) noexcept {
    switch (reg) {

    case PPU_SCROLLX: system->open_bus = scroll_x; break;
    case PPU_SCROLLY: system->open_bus = scroll_y; break;

    }
}

const ppu_t::sprite_t* ppu_t::get_sprite(unsigned i) const noexcept {
    return reinterpret_cast<const sprite_t*>(&sprite_ram[i * 4]);
}

void ppu_t::draw_tile(uint8_t tile_i, uint8_t x, uint8_t fy, uint8_t pal, bool hi, bool flipx, bool flipy) noexcept {
    auto rfy = flipy ? 7 - fy : fy;
    auto tile_p0 = chr_ram[tile_i * 16 + rfy * 2 + 0];
    auto tile_p1 = chr_ram[tile_i * 16 + rfy * 2 + 1];

    for (unsigned fx = 0; fx < 8; fx++) {
        auto rfx = flipx ? 7 - fx : fx;
        auto j = (tile_p0 >> rfx & 1) | (tile_p1 >> rfx & 1) << 1;
        auto k = pal_ram[pal * 4 + j];

        if (!(k & 0x80) && (scanline[x + fx & 255] & 0x80))
            scanline[x + fx & 255] = k | hi << 7;
    }
}

void ppu_t::sl_check_sprite(unsigned i) noexcept {
    auto s = get_sprite(i);
    if (s->visible) {
        auto h = s->h16 ? 16 : 8;
        if ((pix_y - s->y & 255) < h)
            sl_spr[sl_spr_i++] = i;
    }
}

void ppu_t::sl_draw_sprite(unsigned i) noexcept {
    if (i < sl_spr_i) {
        auto s = get_sprite(sl_spr[i]);
        auto fy = pix_y - s->y;
        if (s->h16 && s->flipy) fy ^= 8;

        auto tile_i = s->tile + (fy >= 8) & 255;
        fy &= 7;

        draw_tile(tile_i, s->x, fy, 4 + s->pal, s->hi, s->flipx, s->flipy);
    }
}

void ppu_t::sl_draw_tile(unsigned px) noexcept {
    auto py = pix_y - 80;

    auto ppx = px - (scroll_x & ~7) & 255;
    auto ppy = py - scroll_y & 255;

    auto cx = ppx >> 3 & 31;

    auto cy = ppy >> 3 & 31;
    auto fy = ppy >> 0 & 7;

    auto tile_i = map_ram[cy * 32 + cx];
    auto attr = attr_ram[cy * 16 + cx / 2];
    auto tattr = attr >> (cx & 1) * 4;
    auto pal = tattr >> 0 & 3;
    auto flipx = tattr >> 2 & 1;
    auto flipy = tattr >> 3 & 1;

    draw_tile(tile_i, px + (scroll_x & 7) & 255, fy, pal, false, flipx, flipy);
}

void ppu_t::cycle() noexcept {
    if (wait.has_value() && wait.value() == pix_y) {
        system->halt = false;
        wait = std::nullopt;
    }

    if (80 <= pix_y) {
        if (pix_x == 0) {
            std::fill(scanline.begin(), scanline.end(), 0x80);
            sl_spr_i = 0;
        }

        if (pix_x <= 31 && sl_spr_i != 8)
            sl_check_sprite(pix_x);

        if ((pix_x & 3) == 0 && 32 <= pix_x && pix_x <= 56)
            sl_draw_sprite((pix_x - 32) >> 2);

        if ((pix_x & 7) == 4 && 60 <= pix_x)
            sl_draw_tile(pix_x - 68);

        if (pix_x == 299) {
            for (unsigned i = 0; i < FB_WIDTH; i++)
                system->put_video(i, pix_y - 80, scanline[i] & 0x7F);
        }
    }

    if (++pix_x == 300) {
        pix_x = 0;
        if (++pix_y == 256) {
            vsync = true;
            pix_y = 0;
        }
    }
}
