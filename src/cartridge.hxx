#pragma once

#include <cstddef>
#include <cstdint>
#include <stdexcept>
#include <string>
#include <vector>

#define CART_MAGIC "FEMTO-8\0"

struct cartridge_error_t : public std::runtime_error {
    cartridge_error_t(const std::string& msg);
};

struct cartridge_t {
    struct header_t {
        char magic[8];
        uint8_t flags;
        char pad[7];
    };

    header_t header;
    unsigned prg_mask;
    std::vector<uint8_t> prg;

    void load(size_t bin_size, const uint8_t* bin);
};
