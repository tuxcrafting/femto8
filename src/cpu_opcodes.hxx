#pragma once

enum cpu_opcodes_t {
    OP_ld = 0'000,
    OP_st = 0'010,

    OP_ldx = 0'020,
    OP_stx = 0'030,
    OP_ldxa = 0'040,

    OP_cmp = 0'050,
    OP_sub = 0'060,
    OP_sbc = 0'070,

    OP_add = 0'100,
    OP_adc = 0'110,

    OP_bit = 0'120,

    OP_and = 0'130,
    OP_or = 0'140,
    OP_xor = 0'150,

    OP_jmp = 0'160,
    OP_call = 0'170,

    OP_inc = 0'200,
    OP_dec = 0'210,

    OP_shl = 0'220,
    OP_shr = 0'230,
    OP_sar = 0'240,

    OP_not = 0'250,

    OP_rlc = 0'260,
    OP_rrc = 0'270,

    OP_jnc = 0'300,
    OP_jnv = 0'302,
    OP_jnz = 0'304,
    OP_jns = 0'306,

    OP_jc = 0'310,
    OP_jv = 0'312,
    OP_jz = 0'314,
    OP_js = 0'316,

    OP_clc = 0'320,
    OP_cli = 0'321,
    OP_clv = 0'322,
    OP_cld = 0'323,
    OP_clz = 0'324,
    OP_clx = 0'325,
    OP_cls = 0'326,
    OP_cla = 0'327,

    OP_stc = 0'330,
    OP_sti = 0'331,
    OP_stv = 0'332,
    OP_std = 0'333,
    OP_stz = 0'334,
    OP_ldsx = 0'335,
    OP_sts = 0'336,
    OP_stsx = 0'337,

    OP_ret = 0'340,
    OP_iret = 0'341,

    OP_inx = 0'342,
    OP_dex = 0'343,

    OP_txs = 0'344,
    OP_tsx = 0'345,

    OP_brk = 0'346,
    OP_wai = 0'347,

    OP_php = 0'350,
    OP_phx = 0'351,

    OP_txal = 0'352,
    OP_txah = 0'353,

    OP_plp = 0'354,
    OP_plx = 0'355,

    OP_taxl = 0'356,
    OP_taxh = 0'357,

    OP_cxz = 0'360,
};

enum cpu_addr_mode_t {
    ADDR_abs_8 = 0'0,
    ADDR_abs_8s = 0'0,
    ADDR_imm_8 = 0'1,
    ADDR_imm_16 = 0'1,
    ADDR_imp = 0'1,
    ADDR_abs_16 = 0'2,
    ADDR_idx = 0'3,
    ADDR_ind_8 = 0'4,
    ADDR_ind_idx = 0'5,
    ADDR_ind_16 = 0'6,
    ADDR_idx_ind = 0'7,
};
