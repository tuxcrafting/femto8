#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <stdexcept>
#include <string>
#include <vector>
#include "cartridge.hxx"

cartridge_error_t::cartridge_error_t(const std::string& msg)
    : std::runtime_error(msg) { }

void cartridge_t::load(size_t bin_size, const uint8_t* bin) {
    if (bin_size < 16)
        throw cartridge_error_t("invalid size");
    std::copy(
        reinterpret_cast<const std::byte*>(&bin[0]),
        reinterpret_cast<const std::byte*>(&bin[16]),
        reinterpret_cast<std::byte*>(&header));
    if (!std::equal(&header.magic[0], &header.magic[8], CART_MAGIC))
        throw cartridge_error_t("invalid magic number");

    auto prg_shift = header.flags >> 0 & 7;
    if (prg_shift > 4) throw cartridge_error_t("PRG shift too big");

    auto prg_size = 0x2000 << prg_shift;
    if (bin_size != prg_size + 16)
        throw cartridge_error_t("invalid size");

    prg_mask = prg_size - 1;
    prg = std::vector<uint8_t>(prg_size);

    std::copy(&bin[16], &bin[16 + prg_size], prg.begin());
}
