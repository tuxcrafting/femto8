#include <algorithm>
#include <cstdint>
#include <iostream>
#include "agu.hxx"
#include "cartridge.hxx"
#include "cpu.hxx"
#include "ppu.hxx"
#include "system.hxx"
#include "system_defs.hxx"

system_t::system_t()
    : cpu(this)
    , ppu(this)
    , agu(this)
    , audio(AUDIO_QUEUE_SIZE)
    , video_tmp(FB_WIDTH * FB_HEIGHT)
    , video_out(FB_WIDTH * FB_HEIGHT)
    , ram(RAM_SIZE)
    , ctrl(0) {
    audio_i = 0;
    audio_j = 0;
}

void system_t::set_cart(cartridge_t* cart) noexcept {
    this->cart = cart;
}

void system_t::reset() noexcept {
    cpu.reset();
    ppu.reset();
    agu.reset();

    raise = false;
    halt = false;

    mapper[0] = 0 << 13;
    mapper[1] = 1 << 13;
    mapper[2] = 2 << 13;
    mapper[3] = 3 << 13;

    cyc_count = 0;
    cyc_lc = 0;
    cyc_to = 0;

    std::fill(ram.begin(), ram.end(), 0x00);
    std::fill(video_tmp.begin(), video_tmp.end(), 0x00);
    std::fill(video_out.begin(), video_out.end(), 0x00);
}

void system_t::advance() noexcept {
    cyc_count += CPU_DIV;
}

uint8_t system_t::read_prg(uint16_t addr) const noexcept {
    return cart->prg[(mapper[addr >> 13 & 3] | (addr & 0x1FFF)) & cart->prg_mask];
}

void system_t::write8(uint16_t addr, uint8_t data) noexcept {
    open_bus = data;
    if (addr & 0x8000) {
        // ROM
    } else if (addr & 0x4000) {
        catch_up();
        if (addr & 0x800) {
            agu.audio_ram[addr & AUDIO_RAM_MASK] = data;
        } else if (addr & 0x400) {
            ppu.pal_ram[addr & PAL_RAM_MASK] = data;
        } else if (addr & 0x200) {
            ppu.attr_ram[addr & ATTR_RAM_MASK] = data;
        } else if (addr & 0x100) {
            ppu.sprite_ram[addr & SPRITE_RAM_MASK] = data;
        } else if (addr & 0x40) {
            ppu.write(addr & 0x3F, data);
        } else if (addr & 0x20) {
            agu.write(addr & 0x1F, data);
        } else {
            switch (addr & 0x1F) {
            case SYS_MAP01:
                mapper[0] = (data >> 0 & 15) << 13;
                mapper[1] = (data >> 4 & 15) << 13;
                break;
            case SYS_MAP23:
                mapper[2] = (data >> 0 & 15) << 13;
                mapper[3] = (data >> 4 & 15) << 13;
                break;
            case SYS_SERIAL_OUT:
                std::printf("%c", data);
                break;
            }
        }
    } else if (addr & 0x2000) {
        catch_up();
        if (addr & 0x1000) {
            ppu.map_ram[addr & MAP_RAM_MASK] = data;
        } else {
            ppu.chr_ram[addr & CHR_RAM_MASK] = data;
        }
    } else {
        ram[addr & RAM_MASK] = data;
    }
}

uint8_t system_t::read8(uint16_t addr) noexcept {
    if (addr & 0x8000) {
        open_bus = read_prg(addr);
    } else if (addr & 0x4000) {
        catch_up();
        if (addr & 0x800) {
            open_bus = agu.audio_ram[addr & AUDIO_RAM_MASK];
        } else if (addr & 0x400) {
            open_bus = ppu.pal_ram[addr & PAL_RAM_MASK];
        } else if (addr & 0x200) {
            open_bus = ppu.attr_ram[addr & ATTR_RAM_MASK];
        } else if (addr & 0x100) {
            open_bus = ppu.sprite_ram[addr & SPRITE_RAM_MASK];
        } else if (addr & 0x40) {
            ppu.read(addr & 0x3F);
        } else if (addr & 0x20) {
            agu.read(addr & 0x1F);
        } else {
            switch (addr & 0x1F) {
            case SYS_CTRL0: open_bus = ctrl >> 0 & 0xFF; break;
            case SYS_CTRL1: open_bus = ctrl >> 8 & 0xFF; break;
            case SYS_CTRL2: open_bus = ctrl >> 16 & 0xFF; break;
            case SYS_CTRL3: open_bus = ctrl >> 24 & 0xFF; break;
            }
        }
    } else if (addr & 0x2000) {
        if (addr & 0x1000) {
            open_bus = ppu.map_ram[addr & MAP_RAM_MASK];
        } else {
            open_bus = ppu.chr_ram[addr & CHR_RAM_MASK];
        }
    } else {
        open_bus = ram[addr & RAM_MASK];
    }
    return open_bus;
}

void system_t::put_video(uint8_t x, uint8_t y, uint8_t p) noexcept {
    video_tmp[y * FB_WIDTH + x] = p;
}

void system_t::put_audio(float x) noexcept {
    audio[audio_i++] = x;
    audio_i &= AUDIO_QUEUE_MASK;
}

float system_t::get_audio() noexcept {
    if (audio_i == audio_j)
        return 0;
    auto x = audio[audio_j++];
    audio_j &= AUDIO_QUEUE_MASK;
    return x;
}

void system_t::nocpu_cycle() noexcept {
    if (cyc_lc % AGU_DIV == 0)
        agu.cycle();
    ppu.cycle();

    if (ppu.vsync) {
        std::copy(video_tmp.begin(), video_tmp.end(), video_out.begin());
        ppu.vsync = false;
    }

    cyc_lc++;
}

void system_t::catch_up() noexcept {
    while (cyc_lc < cyc_count) {
        nocpu_cycle();
    }
}

void system_t::step() noexcept {
    cpu.step();
}

void system_t::run(int n) noexcept {
    cyc_to += n;
    while (cyc_count < cyc_to) {
        if (halt) {
            advance();
            catch_up();
        } else {
            step();
        }
    }
    catch_up();
}
