#pragma once

#include <SDL2/SDL.h>
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <unordered_map>
#include "cartridge.hxx"
#include "system.hxx"
#include "util.hxx"

struct sdl_error_t : public std::runtime_error {
    sdl_error_t(const std::string& fn);
};

struct frontend_t {
    std::unique_ptr<SDL_Window, deleter<SDL_DestroyWindow>> window;
    std::unique_ptr<SDL_Renderer, deleter<SDL_DestroyRenderer>> renderer;
    std::unique_ptr<SDL_Texture, deleter<SDL_DestroyTexture>> fbtex;
    SDL_AudioDeviceID audiodev;

    std::unordered_map<SDL_Keycode, uint32_t> keymap;

    system_t system;
    bool running;

    frontend_t();

    void resize(unsigned w, unsigned h);
    void do_key(SDL_Keycode key, bool down);

    void handle(const SDL_Event& ev);
    void frame(int delta);

    void mainloop();
};
