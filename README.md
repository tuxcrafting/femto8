# FEMTO-8

FEMTO-8 is a fantasy console inspired by the 2nd and 3rd generation of
home consoles.

## Specifications

- RAM: 8 KiB RAM
- ROM: Up to 128 KiB, banked
- Video: 232x176 pixels with 113 colors at 60 Hz
- Audio: 4 channels of 4-bit PCM at 12 kHz
- CPU: 8-bit processor at 2.304 MHz
- Input: 4 8-button controllers (D-pad + start + A, B, C)

## Build

The Makefile in the root directory builds the example ROMs using
fasmg, so you'll need that installed. Then, use CMake to build the
actual emulator, which requires a recent version of clang and SDL2.

## Usage

The emulator takes arguments in a `key value` format describing the
configuration.

- `rom` - Load the ROM at the specified path.
- `scale` - Scale the window by this number relative to the
  192x160 resolution.
- `keys` - Define the controller mapping, as a semicolon-separated
  list of colon-separated pairs. Each pair is of an [SDL
  keycode](https://wiki.libsdl.org/SDL_Keycode) (backslashes are used
  as an escape including for spaces) and the name of the matching
  controller input (`Left`, `Up`, `Right`, `Down`, `Start`, `A`, `B`,
  `C` and digit 0 to 3).
