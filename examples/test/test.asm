include "../../asm/femto8.inc"

rom_header

virtual at 0x0000
memcpy_src rw 1
memcpy_dst rw 1
memcpy_len rw 1
end virtual

macro do_key i, char
	local no, wr
	php
	bit 1 shl i
	jz no
	ld char
	jmp wr
no:
	ld '-'
wr:
	stsx
	plp
end macro

macro do_audio k, i
	local no
	php
	bit k
	cla
	jz no
	ld 0x08
no:
	st [AGU_C#i#VOL]
	plp
end macro

	org 0xE000
main:
	ld 0x73
	st [PAL_RAM + 1]
	st [PAL_RAM + 2]
	st [PAL_RAM + 3]

	cla
	st [PPU_SCROLLX]
	st [PPU_SCROLLY]

	; copy chr
	ldx chr_data
	stx [memcpy_src]
	ldx CHR_RAM
	stx [memcpy_dst]
	ldx chr_data.end - chr_data
	stx [memcpy_len]
	call memcpy

	; put header
	ldx hello
	stx [memcpy_src]
	ldx MAP_RAM + 0 * 32 + 0
	stx [memcpy_dst]
	ldx hello.end - hello
	stx [memcpy_len]
	call memcpy

	; put palette display
repeat 8, i:0
	ld 1
	st [MAP_RAM + (2 + i) * 32 + 2]
	st [MAP_RAM + (2 + i) * 32 + 4]
	ld 2
	st [MAP_RAM + (2 + i) * 32 + 3]
	st [MAP_RAM + (2 + i) * 32 + 5]

	ld 0x22
	st [ATTR_RAM + (2 + i) * 16 + 1]
	ld 0x33
	st [ATTR_RAM + (2 + i) * 16 + 2]
end repeat

	; copy audio
	ldx audio_data
	stx [memcpy_src]
	ldx AUDIO_RAM
	stx [memcpy_dst]
	ldx audio_data.end - audio_data
	stx [memcpy_len]
	call memcpy

	; init AGU
	cla
	st [AGU_C0BASE]
	st [AGU_C1BASE]
	st [AGU_C2BASE]
	ld (audio_data.end - audio_data) * 2 - 1
	st [AGU_C0LEN]
	st [AGU_C1LEN]
	st [AGU_C2LEN]
	ld 0x08
	st [AGU_C0SPEED]
	ld 0x10
	st [AGU_C1SPEED]
	ld 0x20
	st [AGU_C2SPEED]

wait_vsync:
	cla
	st [PPU_WAIT]

vsync:
repeat 4, i:0
	ld [SYS_CTRL#i]
	ldx MAP_RAM + (2 + i) * 32 + 29 - 8
	call do_ctrl
	if i = 0
		do_audio K_A, 0
		do_audio K_B, 1
		do_audio K_C, 2
	end if
end repeat

	ldx 0x1000
	stx [PAL_RAM + 2 * 4 + 0]
	ldx 0x3020
	stx [PAL_RAM + 2 * 4 + 2]
	ldx 0x5040
	stx [PAL_RAM + 2 * 4 + 4]
	ldx 0x7060
	stx [PAL_RAM + 2 * 4 + 6]

	ld 80 + 8 * 2 + 4
	st [PPU_WAIT]

hsync:
repeat 8, i:0
	inc [PAL_RAM + 2 * 4 + i]
end repeat

	add 4
	cmp 80 + 8 * 2 + 4 + (8 * 8)
	jz wait_vsync

	st [PPU_WAIT]
	jmp hsync

do_ctrl:
	do_key 0, '<'
	do_key 1, '^'
	do_key 2, '>'
	do_key 3, 'v'
	do_key 4, '*'
	do_key 5, 'A'
	do_key 6, 'B'
	do_key 7, 'C'
	ret

	; copy [memcpy_len] bytes from [memcpy_src] to [memcpy_dst] (in reverse!)
memcpy:
.loop:
	ldx [memcpy_len]
	cxz
	jz .end
	dex
	stx [memcpy_len]

	ld *[memcpy_src],x
	st *[memcpy_dst],x

	jmp .loop
.end:
	ret

hello:
	db "---  FEMTO-8 HW test ROM  ---", 0
.end:

audio_data:
	db 0xA8, 0xDB, 0xFE, 0xFF, 0xFF, 0xDE, 0xAB
	db 0x78, 0x45, 0x12, 0x11, 0x11, 0x32, 0x64
.end:

chr_data:
	chr_tile_empty

repeat 8
	chr_tile_line 00001111
end repeat

repeat 8
	chr_tile_line 22223333
end repeat

	chr_tile_empty 30
include "../common/font.inc"
	chr_tile_empty
.end:

repeat 0xFF00 - $
	db 0
end repeat

reset_vec:
	db 162q
	dw main

irq_vec:
	iret

	rom_end
